defmodule Link do
  defstruct [
              url: "",
              inner: :false,
              timesUsed: 0,
              parentURL: "",
              typeTag: :img
            ]

  def instanceLink(url, inner, parentURL, typeTag) do
    %Link{
          url: url,
          inner: inner,
          timesUsed: 1,
          parentURL: parentURL,
          typeTag: typeTag
          }
  end

  def repeatLink(nil), do: nil
  def repeatLink(link) do
    %Link{link | timesUsed: (link.timesUsed + 1)}
  end
end
