defmodule ScrapperTest do
  use ExUnit.Case
  doctest Scrapper

  test "test simple imbalit URL" do
    IO.puts("scrap test")
    response = Scrapper.fetch("httpfffffffffta.com/desarrollo/ibm-lanza-curso-gratuito-espanol-machine-learning-python-que-puedes-comenzar-edx")
    assert response == {:error, "error on query URL to fech"}
  end

  test "test simple Scrap" do
    IO.puts("scrap test")
    {:ok, listLinks, listImg} = Scrapper.fetch("https://www.genbeta.com/desarrollo/ibm-lanza-curso-gratuito-espanol-machine-learning-python-que-puedes-comenzar-edx")
    assert Enum.count(listLinks) == 201
    assert Enum.count(listImg) == 7
  end
end
