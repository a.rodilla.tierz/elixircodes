
defmodule Codes.MongoManager do
  def preserveURLDatas(listDatasURL, collection) do
    Mongo.insert_many(:mongo, collection, listDatasURL)
  end

  def findURLDatas(objQuery, collection) do
    Mongo.find(:mongo, collection, objQuery)
  end

  def deleteURLDatas(objDel, collection) do
    Mongo.delete_one(:mongo, collection, objDel)
  end
end
