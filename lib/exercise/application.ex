defmodule Codes.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  require Logger

  def start(_type, _args) do
    import Supervisor.Spec
    children = [
      # Use Plug.Cowboy.child_spec/3 to register our endpoint as a plug
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: Codes.Router,
        options: [port: Application.get_env(:codes, :port)]
      ),
      worker(Mongo, [[name: :mongo, database: Application.get_env(:codes, :database), seeds: [Application.get_env(:codes, :mongoConfig)], pool_size: 2]])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Codes.Supervisor]
    Logger.info("Launching server")
    Supervisor.start_link(children, opts)
  end
end
