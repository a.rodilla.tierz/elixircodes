use Mix.Config

config :codes, port: 4000

config :codes, database: "tembezaTest"
config :codes, mongoConfig: "localhost:27017"
