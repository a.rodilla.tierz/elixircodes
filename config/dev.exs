use Mix.Config

config :codes, port: 4001

config :codes, database: "tembeza"
config :codes, mongoConfig: "localhost:27017"
