
defmodule Scrapper do
  def fetch(url) do
    etsWorker = initialiterETS(url)
    case HTTPoison.get(url) do
      {:ok, response} -> manageHTML(response, url, etsWorker)
      _ -> IO.puts "error on query URL to fech"
           {:error, "error on query URL to fech"}
    end
  end

  defp initialiterETS(workURL) do
    :ets.new(String.to_atom((inspect workURL <> inspect self())), [:set, :protected])
  end

  defp cleanerETS(etsWorker) do
    :ets.delete(etsWorker)
  end

  defp manageHTML(codeHTML, originalURL, etsWorker) do
    case Floki.parse_document(codeHTML.body()) do
      {:ok, document} -> listLink = linkManager(document, originalURL, etsWorker)
                         listImg = imageManager(document, originalURL, etsWorker)
                         cleanerETS(etsWorker)
                         {:ok, listLink, listImg}
      _ -> cleanerETS(etsWorker)
           {:error, "not found structure in html"}
    end
  end

  defp linkManager(document, originalURL, etsWorker) do
    document
    |> Floki.find("a")
    |> Enum.reduce([], fn x, acc -> logicLink(x, acc, originalURL, etsWorker)
                       end)
  end

  defp imageManager(document, originalURL, etsWorker) do
    document
    |> Floki.find("img")
    |> Enum.reduce([], fn x, acc -> logicImage(x, acc, originalURL, etsWorker)
                       end)
  end

  defp logicLink(nodeA, acc, originalURL, etsWorker) do
    nodeA
    |> Floki.attribute("href")
    |> Enum.at(0)
    |> logicInnerAccress
    |> checkPreviusLink(etsWorker, :a, originalURL, acc)
  end

  defp logicImage(nodeIMG, acc, originalURL, etsWorker) do
    nodeIMG
    |> Floki.attribute("src")
    |> Enum.at(0)
    |> logicInnerAccress
    |> checkPreviusLink(etsWorker, :img, originalURL, acc)
  end

  defp logicInnerAccress(value) do
    case String.at(value, 0) == "/" do
      true -> {:true, (String.slice(value, 1, (String.length(value) - 1)))}
      false -> {:false, value}
    end
  end

  defp checkPreviusLink({inner, url}, etsWorker, type, parentURL, acc) do
    case :ets.lookup(etsWorker, url) do
      [] -> :ets.insert_new(etsWorker, {url, Enum.count(acc)})
            [Link.instanceLink(url, inner, parentURL, type) | acc]
      [{_, indexAcc}] -> newLink = acc |> Enum.at(indexAcc) |> Link.repeatLink
                               acc |> List.replace_at(indexAcc, newLink)
      _ -> acc
    end
  end
end
