defmodule Codes.Router do
  use Plug.Router
  require Logger

  plug :match
  plug :dispatch
  plug(Plug.Parsers, parsers: [:json], json_decoder: Poison)

  get "/" do
    send_resp(conn, 200, "Welcome, de server is ready")
  end

  post "/fetchURL" do
    conn
    |> read_body
    |> Codes.ControllerFetch.managePostFetch
    |> manageResponse
    |> case do
        {200, response} -> send_resp(conn, 200, ("fetch success for " <> Integer.to_string(response) <> " URL's found"))
        {201, response} -> send_resp(conn, 201, ("error at proccesby: " <> response))
        {500, _} -> send_resp(conn, 500, "Error at server")
      end
  end

  defp manageResponse({:error, datas_resp}), do: {201, ("error at proccesby: " <> datas_resp)}
  defp manageResponse({:ok, datas_resp}), do: {200, datas_resp}
  defp manageResponse(_), do: {500, "Error at server"}

  match _ do
    send_resp(conn, 404, "Worng way")
  end
end
