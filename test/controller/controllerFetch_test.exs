defmodule Codes.ControllerFetchTest do
  use ExUnit.Case
  doctest Codes.ControllerFetch

  test "test simple controller" do
    assert Codes.ControllerFetch.managePostFetch({:error, "foo", "foo2"}) == {:error, "connection body can't read"}
    urlSimple = "{\"url\": \"https://www.genbeta.com/desarrollo/ibm-lanza-curso-gratuito-espanol-machine-learning-python-que-puedes-comenzar-edx\"}"
    responseController = Codes.ControllerFetch.managePostFetch({:ok, urlSimple, "foo"})
    assert responseController == {:ok, 208}
  end

  test "test simple list" do
    urlSimple = "{\"url\": [\"https://www.genbeta.com/desarrollo/ibm-lanza-curso-gratuito-espanol-machine-learning-python-que-puedes-comenzar-edx\", \"https://dev.to/jonlunsford/elixir-building-a-small-json-endpoint-with-plug-cowboy-and-poison-1826\"]}"
    responseController = Codes.ControllerFetch.managePostFetch({:ok, urlSimple, "foo"})
    assert responseController == {:ok, 408}
  end

  test "test erro encode" do
    urlSimple = "{\"url\"fgthsdhsdrghsga-curso-gratuito-espanol-machine-fgtxdxderjx/elixir-building-a-small-json-endpoint-with-plug-cowboy-and-poison-1826\"]}"
    responseController = Codes.ControllerFetch.managePostFetch({:ok, urlSimple, "foo"})
    assert responseController == {:error, "bad format on body param"}
  end

  test "test error in just one URL" do
    urlSimple = "{\"url\": [\"https://www.genbeta.com/desarrollo/ibm-lanza-curso-gratuito-espanol-machine-learning-python-que-puedes-comenzar-edx\", \".to/jonlunsford/elixir-building-a-small-json-endpoint-with-plug-cowboy-and-poison-1826\"]}"
    responseController = Codes.ControllerFetch.managePostFetch({:ok, urlSimple, "foo"})
    assert responseController == {:ok, 208}
  end
end
