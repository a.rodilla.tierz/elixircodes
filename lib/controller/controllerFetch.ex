defmodule Codes.ControllerFetch do

  def managePostFetch({:error, _, _}), do: {:error, "connection body can't read"}
  def managePostFetch({:ok, data, _}) do
    data
    |> Poison.decode
    |> manageFetchQuery
  end

  defp manageFetchQuery({:ok, %{"url" => linksURL}}) when is_list(linksURL) do
    finalVal = Enum.reduce(linksURL, 0, fn x, acc ->
                                          case manageFetchQuery({:ok, %{"url" => x}}) do
                                            {:ok, response} -> acc + response
                                            _ -> acc
                                          end
                                        end)
    {:ok, finalVal}
  end
  defp manageFetchQuery({:ok, %{"url" => linksURL}}) when is_bitstring(linksURL) do
    linksURL
    |> Scrapper.fetch
    |> managePersistence
  end
  defp manageFetchQuery(_), do: {:error, "bad format on body param"}

  defp managePersistence({:ok, listLink, listImg}) when is_list(listLink) and is_list(listImg) do
    {resultInsertLink, idsLink} = listLink
                                  |> Codes.MongoManager.preserveURLDatas("tembezaURL")
    {resultInserImg, idsImg} = listImg
                               |> Codes.MongoManager.preserveURLDatas("tembezaURL")
     {:ok, (Enum.count(idsLink.inserted_ids) + Enum.count(idsImg.inserted_ids))}
  end
  defp managePersistence(x), do: x

end
