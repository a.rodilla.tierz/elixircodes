defmodule MongoManagerTest do
  use ExUnit.Case
  doctest Codes.MongoManager

  test "test Mongo Conection" do
    {:ok, reportMongo} = Codes.MongoManager.preserveURLDatas([%{testKey: "foo"}], "testCollection")
    assert Enum.count(reportMongo.inserted_ids) == 1

    responseFind = Codes.MongoManager.findURLDatas(%{testKey: "foo"}, "testCollection")
    sumTest = responseFind
              |> Enum.to_list
              |> Enum.count
    assert sumTest == 1

    {:ok, reportDelete} = Codes.MongoManager.deleteURLDatas(%{testKey: "foo"}, "testCollection")
    assert reportDelete.deleted_count == 1

    responseFind = Codes.MongoManager.findURLDatas(%{testKey: "foo"}, "testCollection")
    sumTest = responseFind
              |> Enum.to_list
              |> Enum.count
    assert sumTest == 0
  end
end
